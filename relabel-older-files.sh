#!/usr/bin/env bash

# Check if correct number of arguments are passed in
if [[ ! $# -eq 1 ]] ; then
    echo ""
    echo "😱 Please pass in the root render folder as argument"
    echo "=> $0 [root render folder]"
    echo "Example:"
    echo "=> $0 /Volumes/lacie-raid-1/renders/renders"
    echo ""
    echo "Exiting script…"
    echo ""
    exit 0
fi

########################
### Constants        ###
########################
# Make sure the passed in folder doesn't have a trailing slash
temp_root_render_path="${1%/}"
root_render_folder_path="$(realpath "$temp_root_render_path")"

script_log="/Volumes/lacie-raid-1/renders/renders/RelabelOlderFilesScriptLog.log"

### End of constants ###
########################

# ----------------------------------------------------------------------

#################
### Functions ###
#################

### Import logThis function
source ./lib/logthis.sh

### Import startup functions for launching Indesign and checking dependencies and indices
source ./lib/startupfunctions.sh

### Import Ram Disk functions
source ./lib/ramdiskfunctions.sh

### Import render pdf functions
source ./lib/relabelfunctions.sh

# The startTraversingTheIndexTree function assumes the root index "_render_folders.index" is in place
# and starts visiting the render folders one by one. In each folder it checks for the "_files_left_to_relabel.index"
# and, if it finds it, sends every missing volume number to the renderFunction.
#
# The startTraversingTheIndexTree function should receive 1 argument
# $1 is the path to the root render folder without trailing slash.
function startTraversingTheIndexTree {
    while IFS= read -r sub_folder || [ -n "$sub_folder" ]; do
        logThis "0" "Now inside subfolder $sub_folder" 1

        if [ -f "$sub_folder/_files_left_to_relabel.index" ] ; then
            while IFS= read -r file_to_relabel || [ -n "$file_to_relabel" ]; do
                logThis "0" "Sending file $file_to_relabel to relabelFunction" 1
                relabelFunction $sub_folder $file_to_relabel
            done < "$sub_folder/_files_left_to_relabel.index"
        else
            logThis "0" "Found no _files_left_to_relabel.index in $sub_folder. Moving on…"
        fi

    done < "$root_render_folder_path/_render_folders.index"
}

### End of functions ###
########################

# ----------------------------------------------------------------------

######################
### Main execution ###
######################

# Empty log file. For debugging and dev mode.
echo "" > "$script_log"

logThis "1" "Script started by [$user_name]" 1

checkIfDependenciesAreInstalled
createRamDiskAndCopyFiles
#ramDiskForDevMode "RamDisk-2019-03-22_11.29.08"

startTraversingTheIndexTree "$root_render_folder_path"

logThis "1" "Script reached the end. Exiting." 1
### End of main execution ###
#############################