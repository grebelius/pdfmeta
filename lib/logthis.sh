#!/bin/sh

script_name=`basename "$0"`
user_name=`whoami`
log_level_array=(DEBUG INFO ERROR)

function logThis {
    local timeAndDate=$(date '+%Y-%m-%d %H.%M.%S')
    local logLevel="${log_level_array[$1]}"
    local function_name="${FUNCNAME[1]}"
    local message="$2"
    local echoToTerminal="$3"
    echo "[$user_name] [$timeAndDate] > $script_name > $function_name [$logLevel] $message" >> $script_log
    if [ "$echoToTerminal" ] ; then
        echo "[$timeAndDate] > $function_name [$logLevel] $message"
    fi
}

export -f logThis