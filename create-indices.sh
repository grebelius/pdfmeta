#!/usr/bin/env bash


# Check if correct number of arguments are passed in
if [[ ! $# -eq 3 ]] ; then
    echo ""
    echo "😱 Please pass in the root render folder as argument 1,"
    echo "the increment_constant as argument 2 and which indices"
    echo "should be rebuilt (missing_files, relabeled_files or both)"
    echo "as argument 3."
    echo "=> $0 [root render folder] [increment_constant] [indices to rebuild]"
    echo "Example:"
    echo "=> $0 /Volumes/lacie-raid-1/renders/renders 1 both"
    echo ""
    echo "Exiting script…"
    echo ""
    exit 0
fi

# Make sure the passed in folder doesn't have a trailing slash
temp_root_render_folder_path="${1%/}"
root_render_folder_path="$(realpath "$temp_root_render_folder_path")"

# Constant defining increments for the number sequence used by GNU Parallel.
# Should be set to 1.
# Set, for example, to 1000 when debugging to force Parallel to just check every 1000th file.
increment_constant="$2"

# Constant defining which indices to rebuild. missing_files, relabeled_files or both.
indices_to_rebuild_constant="$3"

# And warn about the increment constant if it's set to anything other tha 1
if [ "$increment_constant" != "1" ] ; then
    echo ""
    echo "  😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱"
    echo "  FYI, the increment constant is set to $increment_constant."
    echo "  This is useful for debugging but will not scan every file."
    echo "  Set increment_constant to 1."
    echo "  😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱😱"
    echo ""
fi

##########################
### Start of functions ###
##########################

function checkIfDependenciesAreInstalled {
    echo "Checking dependencies…"

    dependencies_are_missing=false

    if command -v parallel >/dev/null 2>&1 ; then
        echo " ✔︎ GNU Parallel"
    else
        echo " 😱 GNU Parallel (please install with 'brew install parallel')"
        dependencies_are_missing=true
    fi

    if command -v gsed >/dev/null 2>&1 ; then
        echo " ✔︎ GNU Sed"
    else
        echo " 😱 GNU Sed (please install with 'brew install gnu-sed')"
        dependencies_are_missing=true
    fi

    if command -v xattr >/dev/null 2>&1 ; then
        echo " ✔︎ Xattr"
    else
        echo " 😱 Xattr"
        dependencies_are_missing=true
    fi

    if "$dependencies_are_missing" ; then
        echo "Exiting script…"
        echo ""
        exit 1
    fi

    echo ""
}

function createRootIndex {
    echo "Creating root index…"
    if [ -f "$root_render_folder_path/_render_folders.index" ]; then
        rm "$root_render_folder_path/_render_folders.index"
    fi
    ls -d "$root_render_folder_path"/*/ | while IFS= read -r folder; do
        echo "${folder%/}" >> "$root_render_folder_path/_render_folders.index"
    done
    echo ""
}

function createHelperFilesForGnuParallel {
    # Creating files containing all numbers that are supposed to exist in each directory
    echo "Creating helper files for GNU Parallel in sub folders…"
    while IFS= read -r sub_folder || [ -n "$sub_folder" ]; do
        local sub_folder_name=$(basename "$sub_folder")
        local tempArray=(${sub_folder_name//-/ })
        local start_number=${tempArray[0]}
        local end_number=${tempArray[1]}

        seq "$start_number" "$increment_constant" "$end_number" > "$sub_folder/_numberstopipetoparallel.txt"
        if [ "$start_number" -gt 999999 ] || [ "$end_number" -gt 999999 ] ; then
            gsed -i '/1e+06/c\1000000' "$sub_folder/_numberstopipetoparallel.txt"
        fi
    done < "$root_render_folder_path/_render_folders.index"
    echo ""
}


function deleteHelperFilesForGnuParallel {
    # Deleting files containing all numbers that are supposed to exist in each directory
    echo "Deleting helper files…"
    while IFS= read -r sub_folder || [ -n "$sub_folder" ]; do
        if [ -f "$sub_folder/_numberstopipetoparallel.txt" ] ; then
            rm "$sub_folder/_numberstopipetoparallel.txt"
        fi
    done < "$root_render_folder_path/_render_folders.index"
    echo ""
}

function backUpOldIndices {
    echo "Backing up old indices…"
    while IFS= read -r sub_folder || [ -n "$sub_folder" ]; do
        if [ -f "$sub_folder/_missing_files.index" ]; then
	        mv "$sub_folder/_missing_files.index" "$sub_folder/.missing_files_$(date '+%Y-%m-%d_%H.%M.%S').index.bak"
        fi
        if [ -f "$sub_folder/_files_left_to_relabel.index" ]; then
	        mv "$sub_folder/_files_left_to_relabel.index" "$sub_folder/.files_left_to_relabel_$(date '+%Y-%m-%d_%H.%M.%S').index.bak"
        fi
    done < "$root_render_folder_path/_render_folders.index"
    echo ""
}

function createNewMissingFilesIndices {
    echo "Creating new _missing_files.index files…"
    echo "Starting GNU Parallel…"
    while IFS= read -r sub_folder || [ -n "$sub_folder" ]; do
        echo "…in $sub_folder"

        parallel --bar testIfFileExists "$sub_folder" :::: "$sub_folder/_numberstopipetoparallel.txt"

        if [ -f "$sub_folder/_missing_files.index.tmp" ]; then
            sort -u < "$sub_folder/_missing_files.index.tmp" > "$sub_folder/_missing_files.index"
            rm "$sub_folder/_missing_files.index.tmp"
        fi
        
        echo ""
    done < "$root_render_folder_path/_render_folders.index"
    echo ""
}

function createNewFilesLeftToRelabelIndices {
    echo "Creating new _files_left_to_relabel.index files…"
    echo "Starting GNU Parallel…"
    while IFS= read -r sub_folder || [ -n "$sub_folder" ]; do
        echo "…in $sub_folder"

        parallel --bar testIfFileShouldBeRelabeled "$sub_folder" {} :::: "$sub_folder/_numberstopipetoparallel.txt"
        
        if [ -f "$sub_folder/_files_left_to_relabel.index.tmp" ]; then
            sort -u < "$sub_folder/_files_left_to_relabel.index.tmp" > "$sub_folder/_files_left_to_relabel.index"
            rm "$sub_folder/_files_left_to_relabel.index.tmp"
        fi

        echo ""
    done < "$root_render_folder_path/_render_folders.index"
    echo ""
}

# Functions that need to be available to the Parallel run subshells
function testIfFileExists {
    if [ ! -f "$1/One-Trillion-Digits-of-Pi-$2.pdf" ] ; then
        echo "$2" >> "$1/_missing_files.index.tmp"
    fi
}

function testIfFileShouldBeRelabeled {
    # Check if file exists. Return 1 if not.
	if [ ! -f "$1/One-Trillion-Digits-of-Pi-$2.pdf" ]; then
		return 1
	fi

    # Check if file is already relabeled. Return 1 if that's the case.
    if xattr -l "$1/One-Trillion-Digits-of-Pi-$2".pdf | grep -q 'isRelabeled'; then
		return 1
	fi

    # Check if volume number is too low to matter. If so, set isRelabeled to notNeeded.
	if (( "$2" < 2592 )); then
        xattr -w isRelabeled "notNeeded" "$1/One-Trillion-Digits-of-Pi-$2.pdf"
		return 1
	fi

    # Look for the dotfile from an earlier version of the script and remove it if it exists. And set isRelabeled to yes.
	if [ -f "$1/.One-Trillion-Digits-of-Pi-$2.pdf" ]; then
        xattr -w isRelabeled "yes" "$1/One-Trillion-Digits-of-Pi-$2.pdf"
        rm "$1/.One-Trillion-Digits-of-Pi-$2.pdf"
        return 1
	fi

    # If none of the above tests evaluate to true, go ahead and put the file number in the  relabel function.
	echo "$2" >> "$1/_files_left_to_relabel.index.tmp"
}

# Export these functions. Otherwise Parallel will not be able to use them.
export -f testIfFileExists
export -f testIfFileShouldBeRelabeled

### End of functions ###
########################

# ----------------------------------------------------------------------

######################
### Main execution ###
######################

checkIfDependenciesAreInstalled
createRootIndex
createHelperFilesForGnuParallel
backUpOldIndices

## Start generating files according to the indices
if [ "$indices_to_rebuild_constant" = "both" ] || [ "$indices_to_rebuild_constant" = "missing_files" ] ; then
    createNewMissingFilesIndices
fi
if [ "$indices_to_rebuild_constant" = "both" ] || [ "$indices_to_rebuild_constant" = "relabeled_files" ] ; then
    createNewFilesLeftToRelabelIndices
fi

## Clean up afterwards
deleteHelperFilesForGnuParallel

### End of main execution ###
#############################
