#!/usr/bin/env bash

# Detta script är ett gammalt script som jag skrev en gång. Jag sparar det för att kanske använda dear av det framöver.

if [[ ! $# -eq 1 ]] ; then
echo "$0 directory_path"
exit 0
fi

export passed_variable=$1

function overwriteExif () {
	filename=${1##*/}
	filename=${filename%.*}
	number="${filename//[!0-9]/}"
	number=$(printf "%'.f\n" $number)
	exiftool -overwrite_original -Language=en-GB -Author="Lennart Grebelius" -Title="One Trillion Digits of Pi – Volume $number of 1,000,000" "$1"
	echo "created meta posts for $1"
	echo "done processing $1"
}
export -f overwriteExif

function run_parallel_jobs () {
	LC_NUMERIC=en_US
	list=$(echo "$passed_variable/ebook-test-$1??.pdf" )
 	parallel --eta --jobs 0 add_meta_and_linearize_pdf ::: $list
	LC_NUMERIC=sv_SE
}

for i in {1..10000} ; do overwriteExif $i ; done