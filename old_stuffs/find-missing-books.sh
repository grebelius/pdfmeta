#!/usr/bin/env bash

if [[ ! $# -eq 3 ]] ; then
echo "$0 directory_path start_volume end_volume"
exit 0
fi

TEMPDIR="${1%/}"
DIR="$(realpath "$TEMPDIR")"
START="$2"
END="$3"

if [ -f "$DIR/_numberstopipetoparallel.txt" ]; then
	mv "$DIR/_numberstopipetoparallel.txt" "$DIR/.numberstopipetoparallel$(date '+%Y-%m-%d_%H.%M.%S').txt.bak"
fi
if [ -f "$DIR/_missing_files.index" ]; then
	mv "$DIR/_missing_files.index" "$DIR/.missing_files_$(date '+%Y-%m-%d_%H.%M.%S').txt.bak"
fi

seq "$START" "$END" > "$DIR/_numberstopipetoparallel.txt"

testIfFileIsMissing ()
{
	if [ ! -f "$1/One-Trillion-Digits-of-Pi-$2.pdf" ]; then
        echo "$2" >> "$1/_missing_files.index"
		return 1
	fi
}

# Export function so that they are available to the parallel run subshells
export -f testIfFileIsMissing

# Run the jobs in parallel
parallel -j0 --bar testIfFileIsMissing "$DIR" {} :::: "$DIR/_numberstopipetoparallel.txt"

if [ -f "$DIR/_missing_files.index" ]; then
    echo "Found missing files!"
else
	echo "No missing files!"
fi
