# Don't use this repo. Use the fork [pdf-generators](https://gitlab.com/grebelius/pdf-generators)

Scripts to edit metadata in the Pdf versions of our art books

## find_missing_books.sh

A script to search through the folders, looking for missing pdf:s, e.g. books that have not yet been rendered.

```bash
./find_missing_books.sh directory_path start_volume (end_volume)
```

## relabel.sh

This script handles the relabeling.

### Install dependencies

```bash
brew install parallel qpdf
parallel --citation
will cite
```

### Run it

```bash
./relabel.sh directory_path start_volume (end_volume)
```

## Ladda upp filerna på den publika filservern

```bash
rsync -vzP /Volumes/lacie-raid-1/renders/renders/1-100000/1-2591/One* pi@192.168.2.2:/mnt/seagate/www-root/pi
```
