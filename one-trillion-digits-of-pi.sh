#!/usr/bin/env bash

########################
### Constants        ###
########################
RAW_PI_FILES_FOLDER_PATH="/Volumes/lacie-raid-1/million"
PAGENUMBER_FILES_FOLDER_PATH="/Volumes/lacie-raid-1/pagenumbers"
RENDER_FOLDER_PATH="/Volumes/lacie-raid-1/renders/renders"
script_log="/Volumes/lacie-raid-1/renders/renders/ScriptlOut.log"

### Import Ports, IP-address and username to be able to scp to the web server
source "$RENDER_FOLDER_PATH/secret-shell-script-CONSTANTS-for-copying-to-web-server.sh"
folder_path_on_web_server="/media/seagate/static.grebelius.se/pi"

### End of constants ###
########################

# ----------------------------------------------------------------------

#################
### Functions ###
#################

### Import logThis function
source ./lib/logthis.sh

### Import startup functions for launching Indesign and checking dependencies and indices
source ./lib/startupfunctions.sh

### Import Ram Disk functions
source ./lib/ramdiskfunctions.sh

### Import render pdf functions
source ./lib/renderfunctions.sh

# The startTraversingTheIndexTree function assumes the root index "_render_folders.index" is in place
# and starts visiting the render folders one by one. In each folder it checks for the "_missing_files.index"
# and, if it finds it, sends every missing volume number to the renderFunction.
#
# The startTraversingTheIndexTree function should receive no argument
function startTraversingTheIndexTree {
    while IFS= read -r sub_folder || [ -n "$sub_folder" ]; do
        logThis "0" "Now inside subfolder $sub_folder"

        if [ -f "$sub_folder/_missing_files.index" ] ; then
            while IFS= read -r missing_files || [ -n "$missing_files" ]; do
                logThis "0" "Sending missing file $missing_files to renderFunction"
                renderFunction $sub_folder $missing_files
            done < "$sub_folder/_missing_files.index"
        else
            logThis "0" "Found no _missing_files.index in $sub_folder. Moving on…"
        fi

    done < "$RENDER_FOLDER_PATH/_render_folders.index"
}

### End of functions ###
########################

# ----------------------------------------------------------------------

######################
### Main execution ###
######################

# Empty log file. For debugging and dev mode.
echo "" > "$script_log"

logThis "1" "Script started by [$user_name]"

## Launch Indesign and see if everything is in place
#launchIndesign
checkIfDependenciesAreInstalled
checkIfIndicesExists
checkIfFontsAreInstalled
createRamDiskAndCopyFiles
#ramDiskForDevMode "RamDisk-2019-03-22_11.29.08"

# Start generating files according to the indices

startTraversingTheIndexTree

logThis "1" "Script reached the end. Exiting."
### End of main execution ###
#############################