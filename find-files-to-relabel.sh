#!/usr/bin/env bash

# Check if correct number of arguments are passed in
if [[ ! $# -eq 1 ]] ; then
    echo ""
    echo "😱 Please pass in the root render folder as argument"
    echo "=> $0 [root render folder]"
    echo "Example:"
    echo "=> $0 /Volumes/lacie-raid-1/renders/renders"
    echo ""
    echo "Exiting script…"
    echo ""
    exit 0
fi

########################
### Constants        ###
########################
# Make sure the passed in folder doesn't have a trailing slash
temp_root_render_path="${1%/}" && root_render_folder_path="$(realpath "$temp_root_render_path")"
path_rendered_files="$root_render_folder_path/_rendered_files.list"
path_files_that_need_relabeling="$root_render_folder_path/_files_that_need_relabeling.list"

function findFilesThatNeedRelabeling {
    while IFS= read -r file_path || [ -n "$file_path" ]; do
        local xattr_args=(-p isRelabeled "$file_path")
        xattr_result=$(xattr "${xattr_args[@]}")

    	if [ "$xattr_result" != "notNeeded" ] && [ "$xattr_result" != "yes" ]; then
            echo "$file_path" >> "$path_files_that_need_relabeling"
        fi
    done < "$path_rendered_files"
}

function sortResultsFile {
    local sort_args=(-u "$path_files_that_need_relabeling")
    sort -u < "$path_files_that_need_relabeling" > "$path_files_that_need_relabeling.tmp"
    rm "$path_files_that_need_relabeling"
    mv "$path_files_that_need_relabeling.tmp" "$path_files_that_need_relabeling"
}

findFilesThatNeedRelabeling
sortResultsFile